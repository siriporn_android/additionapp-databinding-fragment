package buu.isiriporn.additionapp

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import buu.isiriporn.additionapp.databinding.FragmentTitleBinding
import kotlin.random.Random

class TitleFragment : Fragment() {
    private lateinit var binding: FragmentTitleBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentTitleBinding>(
            inflater,
            R.layout.fragment_title,
            container,
            false
        )
//        timer()
        Math()
        return binding.root
    }

    var correctScore: Int = 0
    var incorrectScore: Int = 0
    var numberOfClauses:Int = -1

//    fun timer () {
//        object : CountDownTimer(31000, 1000) {
//            override fun onTick(millisUntilFinished: Long) {
//                binding.txtTimer.setText("Time   :  " + millisUntilFinished / 1000)
//            }
//            override fun onFinish() {
////                binding.txtTimer.setText("Time's OUT !")
////                Toast.makeText(this@MainActivity, "Game is over" , Toast.LENGTH_LONG).show()
////                val intent = Intent(this@MainActivity, ScoreActivity:: class.java)
////                intent.putExtra("sum", numberOfClauses.toString())
////                intent.putExtra("correct", correctScore.toString())
////                intent.putExtra("incorrect", incorrectScore.toString())
////                startActivity(intent)
//            }
//        }.start()
//    }

    fun Math() {
        binding.apply {
            numberOfClauses++
            val firstNumRand = Random.nextInt(0, 99)
            txtNumber1.text = firstNumRand.toString()

            val secondNumRand = Random.nextInt(0, 99)
            txtNumber2.text = secondNumRand.toString()

            txtSum.text = numberOfClauses.toString()

            val answer: Int = firstNumRand + secondNumRand

            val position = Random.nextInt(0, 3)

            if (position == 1) {
                btnFirstChoice.text = (answer + 10).toString()
                btnSecondChoice.text = (answer - 1).toString()
                btnThirdChoice.text = answer.toString()
            } else if (position == 2) {
                btnFirstChoice.text = (answer - 1).toString()
                btnSecondChoice.text = answer.toString()
                btnThirdChoice.text = (answer + 10).toString()
            } else {
                btnFirstChoice.text = answer.toString()
                btnSecondChoice.text = (answer - 1).toString()
                btnThirdChoice.text = (answer + 10).toString()
            }

            txtpointRight.text = correctScore.toString()
            txtpointWrong.text = incorrectScore.toString()


            fun addCorrect(score: Int) {
                correctScore++
                binding.txtpointRight.text = correctScore.toString()
                txtShowCorrectText.text = " PING PONG! Alright!! "
            }

            fun addIncorrect(score: Int) {
                incorrectScore++
                binding.txtpointWrong.text = incorrectScore.toString()
                txtShowCorrectText.text = " BHU BHUU! It's wrong!!"
            }

            btnFirstChoice.setOnClickListener {
                if (btnFirstChoice.text.toString() == answer.toString()) {
                    addCorrect(correctScore)
                    Math()
                } else {
                    addIncorrect(incorrectScore)
                    Math()
                }
            }

            btnSecondChoice.setOnClickListener {
                if (btnSecondChoice.text.toString() == answer.toString()) {
                    addCorrect(correctScore)
                    Math()
                } else {
                    addIncorrect(incorrectScore)
                    Math()
                }
            }

            btnThirdChoice.setOnClickListener {
                if (btnThirdChoice.text.toString() == answer.toString()) {
                    addCorrect(correctScore)
                    Math()
                } else {
                    addIncorrect(incorrectScore)
                    Math()
                }
            }
        }
    }
}