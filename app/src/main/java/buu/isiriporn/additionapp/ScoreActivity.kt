package buu.isiriporn.additionapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.isiriporn.additionapp.databinding.ActivityScoreBinding
import buu.isiriporn.additionapp.databinding.FragmentScoreBinding

class ScoreActivity : AppCompatActivity() {
    private lateinit var binding: FragmentScoreBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_score)
    }
}